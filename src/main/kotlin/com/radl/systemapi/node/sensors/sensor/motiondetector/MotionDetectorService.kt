package com.radl.systemapi.node.sensors.sensor.motiondetector

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.NodeService
import com.radl.systemapi.node.sensors.sensor.SensorService
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

/**
 * Handling of all operations on Motion Detector sensors
 */
@Service
class MotionDetectorService(
        private val motionDetectorDBRepository: MotionDetectorDBRepository,
        nodeService: NodeService
): SensorService<MotionDetectorDB, Boolean>(
        motionDetectorDBRepository,
        nodeService
) {

    /**
     * Create a new instance for a motion detector log
     *
     * @param node  The related Sensor Node
     * @param value The new value from the motion detector sensor
     * @return The new log instance
     */
    override fun newInstance(node: NodeDB, value: Boolean): MotionDetectorDB = MotionDetectorDB(node, value)

    /**
     * Get the last log for the motion sensor of given a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @return The last log if exists, otherwise null
     */
    private fun getLastLog(serialNumber: String) =
            this.motionDetectorDBRepository.findFirstByNode_SerialNumberOrderByTimeDesc(serialNumber)

    /**
     * Log a new value for the motion sensor of given a Sensor Node
     *
     * Also checks for coherence with persisted data (cannot have 2 consecutive true / false)
     *
     * @param serialNumber Unique serial number of the device
     * @param value        The new value from the sensor
     */
    override fun log(serialNumber: String, value: Boolean) {
        val log = this.getLastLog(serialNumber)

        if (log != null && log.value == value) {
            if (value) super.log(serialNumber, !value, log.time.plusSeconds(3))
            else super.log(serialNumber, !value, OffsetDateTime.now().minusSeconds(3))
        }

        super.log(serialNumber, value)
    }
}
