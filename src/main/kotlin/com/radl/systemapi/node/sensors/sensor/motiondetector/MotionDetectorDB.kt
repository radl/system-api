package com.radl.systemapi.node.sensors.sensor.motiondetector

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.sensors.sensor.SensorDB
import javax.persistence.Entity
import javax.persistence.Table

/**
 * PostgreSQL 'motion_detector' table
 */
@Entity
@Table(name = "motion_detector", schema = "radl")
class MotionDetectorDB(node: NodeDB, value: Boolean): SensorDB<Boolean>(node, value)

