package com.radl.systemapi.node.sensors.sensor

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.NodeService
import java.time.OffsetDateTime

/**
 * Base handling of all operations on a sensor
 *
 * Needs to be derived for all sensors
 */
abstract class SensorService<S: SensorDB<T>, T> (
        private val sensorDBRepository: SensorDBRepository<S>,
        private val nodeService: NodeService
) {

    /**
     * Create a new instance for a sensor log
     *
     * Needs to be implemented by each sensor
     *
     * @param node  The related Sensor Node
     * @param value The new value from the sensor
     * @return The new log instance
     */
    protected abstract fun newInstance(node: NodeDB, value: T): S

    /**
     * Log a new value for the sensor of given a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @param value        The new value from the sensor
     * @param time         Time of the update
     */
    fun log(serialNumber: String, value: T, time: OffsetDateTime? = null) {
        val node: NodeDB? = nodeService.fetchNode(serialNumber)
        if (node != null) {
            sensorDBRepository.save(
                    if (time != null) this.newInstance(node, value).apply { this.time = time }
                    else this.newInstance(node, value)
            )
            nodeService.updateNode(node)
        }
    }

    /**
     * Log a new value for the sensor of given a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @param value        The new value from the sensor
     */
    open fun log(serialNumber: String, value: T) = this.log(serialNumber, value, null)

    /**
     * Fetch all the sensor logs given a Sensor Node
     *
     * Sorts the logs by most recent to oldest
     *
     * @param serialNumber Unique serial number of the device
     * @return The list of sensor logs (DTO)
     */
    fun getLogs(serialNumber: String): List<SensorDTO<T>> =
            sensorDBRepository.findAllByNode_SerialNumber(serialNumber)
                    .sortedByDescending { it.time }
                    .map { it.toDTO() }

    /**
     * Remove all the sensor logs from a given Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     */
    fun removeLogs(serialNumber: String) = sensorDBRepository.deleteAllByNode_SerialNumber(serialNumber)
}
