package com.radl.systemapi.utils.database

import java.util.*
import javax.persistence.AttributeConverter
import javax.persistence.Converter

/**
 * Automatic conversion between UUID (Kotlin) and String (database persistence)
 */
@Converter(autoApply = true)
class UuidAttributeConverter : AttributeConverter<UUID, String> {

    override fun convertToDatabaseColumn(uuid: UUID?): String? = uuid?.toString()

    override fun convertToEntityAttribute(uuid: String?): UUID? = uuid?.let { UUID.fromString(it) }
}
