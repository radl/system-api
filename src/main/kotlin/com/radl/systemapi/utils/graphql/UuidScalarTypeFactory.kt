package com.radl.systemapi.utils.graphql

import graphql.language.StringValue
import graphql.schema.*
import org.springframework.beans.factory.FactoryBean
import org.springframework.stereotype.Component
import java.util.*

/**
 * Factory class for conversion between UUID (Kotlin) and String (GraphQL)
 */
@Component
class UuidScalarTypeFactory: FactoryBean<GraphQLScalarType> {

    override fun getObject(): GraphQLScalarType? = GraphQLScalarType.newScalar()
                .name("UUID")
                .description("Kotlin UUID Scalar")
                .coercing(object: Coercing<UUID, String> {

                    @Throws(CoercingParseValueException::class)
                    override fun parseValue(input: Any): UUID {
                        if (input !is String) throw CoercingParseValueException("Incorrect type")
                        try {
                            return UUID.fromString(input)
                        } catch (e: IllegalArgumentException) {
                            throw CoercingParseValueException(e)
                        }
                    }

                    @Throws(CoercingParseLiteralException::class)
                    override fun parseLiteral(input: Any): UUID {
                        if (input !is StringValue) throw CoercingParseLiteralException("Incorrect type")
                        try {
                            return UUID.fromString(input.value)
                        } catch (e: IllegalArgumentException) {
                            throw CoercingParseLiteralException(e)
                        }
                    }

                    @Throws(CoercingSerializeException::class)
                    override fun serialize(dataFetcherResult: Any): String =
                            if (dataFetcherResult is UUID)
                                dataFetcherResult.toString()
                            else
                                throw CoercingSerializeException("Incorrect type")
                })
                .build()


    override fun getObjectType(): Class<*>? = GraphQLScalarType::class.java

    override fun isSingleton(): Boolean = true
}
