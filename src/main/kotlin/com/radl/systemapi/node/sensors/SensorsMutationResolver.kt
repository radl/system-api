package com.radl.systemapi.node.sensors

import com.radl.systemapi.node.sensors.sensor.co2level.CO2LevelService
import com.radl.systemapi.node.sensors.sensor.humidity.HumidityService
import com.radl.systemapi.node.sensors.sensor.lightintensity.LightIntensityService
import com.radl.systemapi.node.sensors.sensor.motiondetector.MotionDetectorService
import com.radl.systemapi.node.sensors.sensor.soundlevel.SoundLevelService
import com.radl.systemapi.node.sensors.sensor.temperature.TemperatureService
import graphql.kickstart.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component

/**
 * GraphQL Mutation Resolver for operations on the sensors of Sensor Nodes
 */
@Component
class SensorsMutationResolver (
        private val temperatureService: TemperatureService,
        private val humidityService: HumidityService,
        private val co2LevelService: CO2LevelService,
        private val lightIntensityService: LightIntensityService,
        private val soundLevelService: SoundLevelService,
        private val motionDetectorService: MotionDetectorService
): GraphQLMutationResolver {

    /**
     * GraphQL Mutation - Log a new temperature value for a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @param value        New temperature value
     */
    fun logTemperature(serialNumber: String, value: Int) = temperatureService.log(serialNumber, value)

    /**
     * GraphQL Mutation - Log a new humidity value for a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @param value        New humidity value
     */
    fun logHumidity(serialNumber: String, value: Int) = humidityService.log(serialNumber, value)

    /**
     * GraphQL Mutation - Log a new CO2 level value for a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @param value        New CO2 level value
     */
    fun logCO2Level(serialNumber: String, value: Int) = co2LevelService.log(serialNumber, value)

    /**
     * GraphQL Mutation - Log a new light intensity value for a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @param value        New light intensity value
     */
    fun logLightIntensity(serialNumber: String, value: Int) = lightIntensityService.log(serialNumber, value)

    /**
     * GraphQL Mutation - Log a new sound level value for a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @param value        New sound level value
     */
    fun logSoundLevel(serialNumber: String, value: Int) = soundLevelService.log(serialNumber, value)

    /**
     * GraphQL Mutation - Log a new state of motion detector for a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @param value        New state of the motion detector
     */
    fun logMotionDetector(serialNumber: String, value: Boolean) = motionDetectorService.log(serialNumber, value)
}
