package com.radl.systemapi.node.sensors.sensor.lightintensity

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.NodeService
import com.radl.systemapi.node.sensors.sensor.SensorService
import org.springframework.stereotype.Service

/**
 * Handling of all operations on Light Intensity sensors
 */
@Service
class LightIntensityService(
        lightIntensityDBRepository: LightIntensityDBRepository,
        nodeService: NodeService
): SensorService<LightIntensityDB, Int>(
        lightIntensityDBRepository,
        nodeService
) {

    /**
     * Create a new instance for a light intensity log
     *
     * @param node  The related Sensor Node
     * @param value The new value from the light intensity sensor
     * @return The new log instance
     */
    override fun newInstance(node: NodeDB, value: Int): LightIntensityDB = LightIntensityDB(node, value)
}
