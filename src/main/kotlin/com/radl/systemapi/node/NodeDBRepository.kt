package com.radl.systemapi.node

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * JPA Repository for SQL operation on 'node' table (NodeDB)
 */
@Repository
interface NodeDBRepository: JpaRepository<NodeDB, Long> {

    /**
     * Fetch all the Sensor Nodes
     *
     * @return All the Sensor Nodes persisted
     */
    override fun findAll(): List<NodeDB>

    /**
     * Fetch a Sensor Node by its serial number
     *
     * @param serialNumber Unique serial number
     * @return The device if found, null otherwise
     */
    fun findBySerialNumber(serialNumber: String): NodeDB?
}
