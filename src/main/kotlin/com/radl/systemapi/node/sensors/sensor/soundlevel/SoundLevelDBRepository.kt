package com.radl.systemapi.node.sensors.sensor.soundlevel

import com.radl.systemapi.node.sensors.sensor.SensorDBRepository
import org.springframework.stereotype.Repository

/**
 * JPA Repository for SQL operation on 'sound_level' table (SoundLevelDB)
 */
@Repository
interface SoundLevelDBRepository: SensorDBRepository<SoundLevelDB>
