package com.radl.systemapi.utils.graphql

import graphql.language.StringValue
import graphql.schema.*
import org.springframework.beans.factory.FactoryBean
import org.springframework.stereotype.Component
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

/**
 * Factory class for conversion between OffsetDateTime (Kotlin) and String (GraphQL)
 */
@Component
class OffsetDateTimeScalarTypeFactory : FactoryBean<GraphQLScalarType> {

    override fun getObject(): GraphQLScalarType? = GraphQLScalarType.newScalar()
            .name("OffsetDateTime")
            .description("Kotlin OffsetDateTime Scalar")
            .coercing(object: Coercing<OffsetDateTime, String> {

                @Throws(CoercingParseValueException::class)
                override fun parseValue(input: Any): OffsetDateTime {
                    if (input !is String) throw CoercingParseValueException("Malformed OffsetDateTime")
                    try {
                        return OffsetDateTime.parse(input, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                    } catch (e: DateTimeParseException) {
                        throw CoercingParseValueException(e)
                    }
                }

                @Throws(CoercingParseLiteralException::class)
                override fun parseLiteral(input: Any): OffsetDateTime {
                    if (input !is StringValue) throw CoercingSerializeException("Malformed OffsetDateTime")
                    try {
                        return OffsetDateTime.parse(input.value, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                    } catch (e: DateTimeParseException) {
                        throw CoercingParseLiteralException(e)
                    }
                }

                @Throws(CoercingSerializeException::class)
                override fun serialize(dataFetcherResult: Any): String =
                        if (dataFetcherResult is OffsetDateTime)
                            dataFetcherResult.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                        else
                            throw CoercingSerializeException("Malformed OffsetDateTime")
            })
            .build()

    override fun getObjectType(): Class<*>? = GraphQLScalarType::class.java

    override fun isSingleton(): Boolean = true
}
