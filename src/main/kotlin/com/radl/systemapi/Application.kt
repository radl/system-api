package com.radl.systemapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

/**
 * Spring Boot Application
 */
@SpringBootApplication
class Application

/**
 * Entry point of the application
 */
fun main(args: Array<String>) {
	runApplication<Application>(*args)
}
