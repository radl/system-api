package com.radl.systemapi.node.sensors.sensor.lightintensity

import com.radl.systemapi.node.sensors.sensor.SensorDBRepository
import org.springframework.stereotype.Repository

/**
 * JPA Repository for SQL operation on 'light_intensity' table (LightIntensityDB)
 */
@Repository
interface LightIntensityDBRepository: SensorDBRepository<LightIntensityDB>
