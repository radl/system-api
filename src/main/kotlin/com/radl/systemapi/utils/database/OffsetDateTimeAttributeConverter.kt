package com.radl.systemapi.utils.database

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import javax.persistence.AttributeConverter
import javax.persistence.Converter

/**
 * Automatic conversion between OffsetDateTime (Kotlin) and String (database persistence)
 */
@Converter(autoApply = true)
class OffsetDateTimeAttributeConverter : AttributeConverter<OffsetDateTime, String> {

    override fun convertToDatabaseColumn(date: OffsetDateTime?): String? =
            date?.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)

    override fun convertToEntityAttribute(date: String?): OffsetDateTime? =
            date?.let { OffsetDateTime.parse(it, DateTimeFormatter.ISO_OFFSET_DATE_TIME) }
}
