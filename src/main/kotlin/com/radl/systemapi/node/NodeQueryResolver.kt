package com.radl.systemapi.node

import graphql.kickstart.tools.GraphQLQueryResolver
import org.springframework.stereotype.Component

/**
 * Graphql Query Resolver for Sensor Nodes
 */
@Component
class NodeQueryResolver (
        private val nodeService: NodeService
): GraphQLQueryResolver {

    /**
     * GraphQL Query - Fetch all Sensor Nodes
     *
     * @return Full list of all persisted Sensor Nodes
     */
    fun nodes(): List<NodeDTO> = nodeService.getNodes()

    /**
     * GraphQL Query - Fetch a specific Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @return The device if found, null otherwise
     */
    fun node(serialNumber: String): NodeDTO? = nodeService.getNode(serialNumber)
}
