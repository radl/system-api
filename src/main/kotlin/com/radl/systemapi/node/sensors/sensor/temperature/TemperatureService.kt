package com.radl.systemapi.node.sensors.sensor.temperature

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.NodeService
import com.radl.systemapi.node.sensors.sensor.SensorService
import org.springframework.stereotype.Service

/**
 * Handling of all operations on Temperature sensors
 */
@Service
class TemperatureService (
        temperatureDBRepository: TemperatureDBRepository,
        nodeService: NodeService
): SensorService<TemperatureDB, Int>(
        temperatureDBRepository,
        nodeService
) {

    /**
     * Create a new instance for a temperature log
     *
     * @param node  The related Sensor Node
     * @param value The new value from the temperature sensor
     * @return The new log instance
     */
    override fun newInstance(node: NodeDB, value: Int): TemperatureDB = TemperatureDB(node, value)
}
