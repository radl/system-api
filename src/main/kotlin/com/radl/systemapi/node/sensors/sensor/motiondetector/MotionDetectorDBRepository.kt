package com.radl.systemapi.node.sensors.sensor.motiondetector

import com.radl.systemapi.node.sensors.sensor.SensorDBRepository
import org.springframework.stereotype.Repository

/**
 * JPA Repository for SQL operation on 'motion_detector' table (MotionDetectorDB)
 */
@Repository
interface MotionDetectorDBRepository: SensorDBRepository<MotionDetectorDB> {

    fun findFirstByNode_SerialNumberOrderByTimeDesc(serialNumber: String): MotionDetectorDB?
}
