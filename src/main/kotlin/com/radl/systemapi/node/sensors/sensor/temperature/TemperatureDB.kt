package com.radl.systemapi.node.sensors.sensor.temperature

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.sensors.sensor.SensorDB
import javax.persistence.Entity
import javax.persistence.Table

/**
 * PostgreSQL 'temperature' table
 */
@Entity
@Table(name = "temperature", schema = "radl")
class TemperatureDB (node: NodeDB, value: Int): SensorDB<Int>(node, value)
