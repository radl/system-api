package com.radl.systemapi.node.sensors.sensor.humidity

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.sensors.sensor.SensorDB
import javax.persistence.Entity
import javax.persistence.Table

/**
 * PostgreSQL 'humidity' table
 */
@Entity
@Table(name = "humidity", schema = "radl")
class HumidityDB(node: NodeDB, value: Int): SensorDB<Int>(node, value)
