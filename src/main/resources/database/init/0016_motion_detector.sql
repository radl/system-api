--liquibase formatted sql

--changeset agenson:create-motion-detector
CREATE TABLE radl.motion_detector (
    id              BIGSERIAL       NOT NULL    PRIMARY KEY,
    node_id         BIGINT          NOT NULL,
    time            VARCHAR(32)     NOT NULL,
    value           BOOLEAN         NOT NULL,
    FOREIGN KEY (node_id) REFERENCES radl.node(id)
);
