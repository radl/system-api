package com.radl.systemapi.node

import java.time.OffsetDateTime
import javax.persistence.*

/**
 * PostgreSQL 'node' table (Sensor Nodes)
 */
@Entity
@Table(name = "node", schema = "radl")
class NodeDB (
        /**
         * Serial number of the device, used as external ID
         */
        @Column(name = "serial_number", unique = true)
        val serialNumber: String,
        /**
         * Name of the device (brand)
         */
        @Column(name = "device_name")
        val deviceName: String
) {
    /**
     * PostgreSQL internal ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = -1

    /**
     * Whether or not the device is connected
     */
    var connected: Boolean = true

    /**
     * Last time that the device was updated
     */
    @Column(name = "last_updated")
    var lastUpdated: OffsetDateTime = OffsetDateTime.now()

    /**
     * Current battery level of the device
     */
    @Column(name = "battery_level")
    var batteryLevel: Int = -1
}
