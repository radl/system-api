package com.radl.systemapi.node.sensors.sensor

import com.radl.systemapi.node.NodeDB
import java.time.OffsetDateTime
import javax.persistence.*

/**
 * Base for PostgreSQL sensor table
 *
 * Needs to be derived for each sensor
 */
@MappedSuperclass
abstract class SensorDB<T> (
        /**
         * Foreign key linking a Sensor Node
         */
        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "node_id", nullable = false)
        val node: NodeDB,
        /**
         * New value from the sensor
         */
        val value: T
) {
    /**
     * PostgreSQL internal ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = -1

    /**
     * Timestamp from new instance creation
     */
    var time: OffsetDateTime = OffsetDateTime.now()
}
