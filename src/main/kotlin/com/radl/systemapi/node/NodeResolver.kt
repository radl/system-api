package com.radl.systemapi.node

import com.radl.systemapi.node.sensors.SensorsDTO
import com.radl.systemapi.node.sensors.SensorsService
import graphql.kickstart.tools.GraphQLResolver
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

/**
 * GraphQL Resolver for Node Sensor
 *
 * The 'sensors' field is resolved only when asked by clients,
 * as it fetches all the sensor values for a given Sensor Node
 */
@Component
class NodeResolver (
        private val sensorsService: SensorsService
): GraphQLResolver<NodeDTO> {

    /**
     * GraphQL Field Resolver - Group of all sensors logs for a given Sensor Node and time range
     *
     * @param node The related Sensor Node (DTO)
     * @param from Beginning of time range
     * @param to Ending of time range
     * @return Group of sensors and respective values
     */
    fun sensors(node: NodeDTO, from: OffsetDateTime?, to: OffsetDateTime?): SensorsDTO =
            sensorsService.getSensors(node, from, to)
}
