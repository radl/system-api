package com.radl.systemapi.node.sensors.sensor.co2level

import com.radl.systemapi.node.sensors.sensor.SensorDBRepository
import org.springframework.stereotype.Repository

/**
 * JPA Repository for SQL operation on 'co2_level' table (CO2LevelDB)
 */
@Repository
interface CO2LevelDBRepository: SensorDBRepository<CO2LevelDB>
