# RADL System API

Implementation of an **API** for the **RADL System**, using the following technologies:
- [Kotlin](https://kotlinlang.org/) Language
- [Gradle](https://gradle.org/) Build Tool
- [Spring Boot](https://spring.io/projects/spring-boot) Framework
- [Liquibase](https://www.liquibase.org/) (Database VCS)
- [PostgreSQL](https://www.postgresql.org/) Database
- [GraphQL](https://graphql.org/) API

## Getting Started

#### Requirements

This project was implemented with the following:
- Java 1.8.0_221
- PostgreSQL 12.2

It is advised to have the same versions to run this project.

#### Setup

The database credentials needs to be provided. By default, it uses environment variables:

```yaml
spring:
  datasource:
    url: ${RADL_DB_URL}
    username: ${RADL_DB_USERNAME}
    password: ${RADL_DB_PASSWORD}
```

Two ways of set the credentials:
- Export the environment variables
- Provide the values directly in `application.yml`

##### Export ENV example
Provide the values as follows in the terminal:
```shell script
export RADL_DB_URL="jdbc:postgresql://localhost:5432/postgres";
export RADL_DB_USERNAME="postgres";
export RADL_DB_PASSWORD="postgres";
```

##### Example application.yml
Provide the values directly in `resources/application.yml`
```yaml
spring:
  datasource:
    url: jdbc:postgresql://localhost:5433/postgres
    username: postgres
    password: postgres
```

#### Usage

To build the project:
```shell script
./gradlew clean
./gradlew build
```

The `build` script will generate a JAR file: `./build/libs/system-api-${__VERSION__}.jar`

An example to run the program:
```shell script
java -jar ./build/libs/system-api-1.0.0.jar
```

The program can also be ran without compilation:

```shell script
./gradlew bootRun
```

## GraphQL API

The clients need to implement GraphQL requests.

A GraphiQL Playground interface is provided at: `http://localhost:4000/graphiql`
