package com.radl.systemapi.node

import graphql.kickstart.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component

/**
 * GraphQL Mutation Resolver for operations on Sensor Nodes
 */
@Component
class NodeMutationResolver (
        private val nodeService: NodeService
): GraphQLMutationResolver {

    /**
     * GraphQL Mutation - Connect a Sensor Node (create if not exists)
     *
     * @param serialNumber Unique serial number of the device
     * @param deviceName   Name of the device (brand)
     * @return The Sensor Node
     */
    fun connectNode(serialNumber: String, deviceName: String): NodeDTO = nodeService.connectNode(serialNumber, deviceName)

    /**
     * GraphQL Mutation - Disconnect a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     */
    fun disconnectNode(serialNumber: String) = nodeService.disconnectNode(serialNumber)

    /**
     * GraphQL Mutation - Remove a Sensor Node from persistence with all its related information
     *
     * @param serialNumber Unique serial number of the device
     */
    fun removeNode(serialNumber: String) = nodeService.removeNode(serialNumber)

    /**
     * GraphQL Mutation - Update the battery level of a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @param value        Battery level
     */
    fun updateBatteryLevel(serialNumber: String, value: Int) = nodeService.updateBatteryLevel(serialNumber, value)
}
