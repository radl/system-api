package com.radl.systemapi.node.sensors

import com.radl.systemapi.node.NodeDTO
import com.radl.systemapi.node.sensors.sensor.SensorDTO
import com.radl.systemapi.node.sensors.sensor.co2level.CO2LevelService
import com.radl.systemapi.node.sensors.sensor.humidity.HumidityService
import com.radl.systemapi.node.sensors.sensor.lightintensity.LightIntensityService
import com.radl.systemapi.node.sensors.sensor.motiondetector.MotionDetectorService
import com.radl.systemapi.node.sensors.sensor.soundlevel.SoundLevelService
import com.radl.systemapi.node.sensors.sensor.temperature.TemperatureService
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

/**
 * Handling of general operations on the sensors of Sensor Nodes
 */
@Service
class SensorsService (
        private val temperatureService: TemperatureService,
        private val humidityService: HumidityService,
        private val co2LevelService: CO2LevelService,
        private val lightIntensityService: LightIntensityService,
        private val soundLevelService: SoundLevelService,
        private val motionDetectorService: MotionDetectorService
) {

    /**
     * Group of all sensors logs for a given Sensor Node and time range
     *
     * @param node The related Sensor Node (DTO)
     * @param from Beginning of time range
     * @param to Ending of time range
     * @return Group of sensors and respective values
     */
    fun getSensors(node: NodeDTO, from: OffsetDateTime?, to: OffsetDateTime?): SensorsDTO {
        val after: (SensorDTO<*>) -> Boolean = { it.time >= from }
        val before: (SensorDTO<*>) -> Boolean = { it.time <= to }
        val sensors = SensorsDTO(
                temperatureService.getLogs(node.serialNumber),
                humidityService.getLogs(node.serialNumber),
                co2LevelService.getLogs(node.serialNumber),
                lightIntensityService.getLogs(node.serialNumber),
                soundLevelService.getLogs(node.serialNumber),
                motionDetectorService.getLogs(node.serialNumber)
        )

        if (from != null) sensors.filter(after)
        if (to != null) sensors.filter(before)

        return sensors
    }

    /**
     * Remove logs from all sensors related to a given Sensor Node
     *
     * @param node The related Sensor Node (DTO)
     */
    fun removeLogs(node: NodeDTO) {
        temperatureService.removeLogs(node.serialNumber)
        humidityService.removeLogs(node.serialNumber)
        co2LevelService.removeLogs(node.serialNumber)
        lightIntensityService.removeLogs(node.serialNumber)
        soundLevelService.removeLogs(node.serialNumber)
        motionDetectorService.removeLogs(node.serialNumber)
    }
}
