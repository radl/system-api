package com.radl.systemapi.node.sensors

import com.radl.systemapi.node.sensors.sensor.SensorDTO

/**
 * DTO version grouping all possible sensors for Sensor Nodes and their respective list of timed values
 *
 * Use of a DTO (Data Transfer Object) version to abstract any internal information about the sensors
 */
class SensorsDTO (
        var temperature: List<SensorDTO<Int>>,
        var humidity: List<SensorDTO<Int>>,
        var co2Level: List<SensorDTO<Int>>,
        var lightIntensity: List<SensorDTO<Int>>,
        var soundLevel: List<SensorDTO<Int>>,
        var motionDetector: List<SensorDTO<Boolean>>
) {

    fun filter(predicate: (SensorDTO<*>) -> Boolean) {
        this.temperature = this.temperature.filter(predicate)
        this.humidity = this.humidity.filter(predicate)
        this.co2Level = this.co2Level.filter(predicate)
        this.lightIntensity = this.lightIntensity.filter(predicate)
        this.soundLevel = this.soundLevel.filter(predicate)
        this.motionDetector = this.motionDetector.filter(predicate)
    }
}
