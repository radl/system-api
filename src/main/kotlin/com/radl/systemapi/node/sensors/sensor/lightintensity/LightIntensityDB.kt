package com.radl.systemapi.node.sensors.sensor.lightintensity

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.sensors.sensor.SensorDB
import javax.persistence.Entity
import javax.persistence.Table

/**
 * PostgreSQL 'light_intensity' table
 */
@Entity
@Table(name = "light_intensity", schema = "radl")
class LightIntensityDB(node: NodeDB, value: Int): SensorDB<Int>(node, value)
