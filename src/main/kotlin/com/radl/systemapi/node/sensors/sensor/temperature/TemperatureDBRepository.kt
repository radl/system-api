package com.radl.systemapi.node.sensors.sensor.temperature

import com.radl.systemapi.node.sensors.sensor.SensorDBRepository
import org.springframework.stereotype.Repository

/**
 * JPA Repository for SQL operation on 'temperature' table (TemperatureDB)
 */
@Repository
interface TemperatureDBRepository: SensorDBRepository<TemperatureDB>
