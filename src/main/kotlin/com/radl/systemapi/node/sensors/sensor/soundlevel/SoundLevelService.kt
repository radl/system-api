package com.radl.systemapi.node.sensors.sensor.soundlevel

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.NodeService
import com.radl.systemapi.node.sensors.sensor.SensorService
import org.springframework.stereotype.Service

/**
 * Handling of all operations on Sound Level sensors
 */
@Service
class SoundLevelService(
        soundLevelDBRepository: SoundLevelDBRepository,
        nodeService: NodeService
): SensorService<SoundLevelDB, Int>(
        soundLevelDBRepository,
        nodeService
) {

    /**
     * Create a new instance for a sound level log
     *
     * @param node  The related Sensor Node
     * @param value The new value from the sound level sensor
     * @return The new log instance
     */
    override fun newInstance(node: NodeDB, value: Int): SoundLevelDB = SoundLevelDB(node, value)
}
