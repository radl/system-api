package com.radl.systemapi.node

import com.radl.systemapi.node.sensors.SensorsService
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import java.time.OffsetDateTime
import java.util.*
import kotlin.concurrent.fixedRateTimer

/**
 * Handling of all operations on Sensor Nodes
 */
@Service
class NodeService(
        private val nodeDBRepository: NodeDBRepository,
        // Lazy annotation added to avoid cyclic autowiring
        @Lazy private val sensorsService: SensorsService
) {

    /**
     * List of scheduled timers for each connected node, checking if the connection is still active
     */
    private val connectionCheckers: MutableMap<String, Timer> = mutableMapOf()

    init {
        // Initialize the list of connection checkers at the start of the program,
        // since some nodes might be persisted as connected
        nodeDBRepository.findAll().forEach {
            if (it.connected) addConnectionChecker(it)
        }
    }

    /**
     * Schedule the checking of a Sensor Node connectivity, if not already done
     *
     * @param node Sensor Node to check
     */
    private fun addConnectionChecker(node: NodeDB) {
        val period: Long = 30000

        if (!connectionCheckers.containsKey(node.serialNumber)) {
            connectionCheckers[node.serialNumber] = fixedRateTimer(node.serialNumber, false, period, period) {
                val nodeDB: NodeDB? = fetchNode(node.serialNumber)

                if (nodeDB == null) removeConnectionChecker(node)
                else if (!nodeDB.connected || OffsetDateTime.now().minusMinutes(1) > nodeDB.lastUpdated) {
                    disconnectNode(node.serialNumber)
                }
            }
        }
    }

    /**
     * Cancel scheduled checking of a Sensor Node connectivity, if exists
     *
     * @param node The Sensor Node
     */
    private fun removeConnectionChecker(node: NodeDB) {
        connectionCheckers[node.serialNumber]?.cancel()
        connectionCheckers.remove(node.serialNumber)
    }

    /**
     * Update a Sensor Node by adding time of modification and connectivity status
     *
     * @param node      Sensor Node to update
     * @param connected Whether or not the device is connected. If connected, it will trigger the checking of
     *                  its connectivity. Otherwise, it will cancel the scheduled checking. Default: `true`
     * @param block     Other modifications on the Sensor Node
     * @return The updated Sensor Node
     */
    private fun updateNode(node: NodeDB, connected: Boolean = true, block: NodeDB.() -> Unit): NodeDB {
        if (connected) addConnectionChecker(node)
        else removeConnectionChecker(node)

        return nodeDBRepository.save(
                node.apply {
                    this.lastUpdated = OffsetDateTime.now()
                    this.connected = connected
                }.apply(block)
        )
    }

    /**
     * Public overloading of `updateNode` method with empty modification block.
     * Used by sensor logs to notify the Sensor Node of its update
     *
     * @param node      Sensor Node to update
     * @param connected Whether or not the device is connected. If connected, it will trigger the checking of
     *                  its connectivity. Otherwise, it will cancel the scheduled checking. Default: `true`
     * @return The updated Sensor Node
     */
    fun updateNode(node: NodeDB, connected: Boolean = true): NodeDB = updateNode(node, connected) { }

    /**
     * Connect a Sensor Node (create if not exists)
     *
     * @param serialNumber Unique serial number of the device
     * @param deviceName   Name of the device (brand)
     * @return The Sensor Node (DTO)
     */
    fun connectNode(serialNumber: String, deviceName: String): NodeDTO =
            updateNode(this.fetchNode(serialNumber) ?: NodeDB(serialNumber, deviceName)).toDTO()

    /**
     * Fetch a specific Sensor Node from database
     *
     * @param serialNumber Unique serial number of the device
     * @return The device if found, null otherwise
     */
    fun fetchNode(serialNumber: String): NodeDB? = nodeDBRepository.findBySerialNumber(serialNumber)

    /**
     * Fetch all Sensor Nodes (DTO)
     *
     * @return Full list of all persisted Sensor Nodes
     */
    fun getNodes(): List<NodeDTO> = nodeDBRepository.findAll().map { it.toDTO() }

    /**
     * Fetch a specific Sensor Node (DTO)
     *
     * @param serialNumber Unique serial number of the device
     * @return The device if found, null otherwise
     */
    fun getNode(serialNumber: String): NodeDTO? = this.fetchNode(serialNumber)?.toDTO()

    /**
     * Disconnect a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     */
    fun disconnectNode(serialNumber: String) {
        this.fetchNode(serialNumber)?.let { updateNode(it, false) }
    }

    /**
     * Remove a Sensor Node from persistence with all its related information
     *
     * @param serialNumber Unique serial number of the device
     */
    fun removeNode(serialNumber: String) {
        this.fetchNode(serialNumber)?.let {
            removeConnectionChecker(it)
            // Also remove all sensor values related to the Sensor Node
            sensorsService.removeLogs(it.toDTO())
            nodeDBRepository.delete(it)
        }
    }

    /**
     * Update the battery level of a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @param value        Battery level
     */
    fun updateBatteryLevel(serialNumber: String, value: Int) {
        this.fetchNode(serialNumber)?.let { updateNode(it) { this.batteryLevel = value} }
    }
}
