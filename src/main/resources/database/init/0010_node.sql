--liquibase formatted sql

--changeset agenson:create-node
CREATE TABLE radl.node (
    id              BIGSERIAL       NOT NULL    PRIMARY KEY,
    serial_number   VARCHAR(20)     NOT NULL    UNIQUE,
    device_name     VARCHAR(20)     NOT NULL,
    connected       BOOLEAN         NOT NULL,
    last_updated    VARCHAR(32)     NOT NULL,
    battery_level   SMALLINT        NOT NULL
);
