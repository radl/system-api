package com.radl.systemapi.node

import java.time.OffsetDateTime

/**
 * Converts a NodeDB object to NodeDTO
 */
fun NodeDB.toDTO() = NodeDTO(
        this.serialNumber,
        this.deviceName,
        this.connected,
        this.lastUpdated,
        this.batteryLevel
)

/**
 * DTO version of NodeDB
 *
 * Use a DTO (Data Transfer Object) version of NodeDB
 * to abstract internal information
 */
class NodeDTO (
        val serialNumber: String,
        val deviceName: String,
        val connected: Boolean,
        val lastUpdated: OffsetDateTime,
        val batteryLevel: Int
)
