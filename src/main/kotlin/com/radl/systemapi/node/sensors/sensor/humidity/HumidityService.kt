package com.radl.systemapi.node.sensors.sensor.humidity

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.NodeService
import com.radl.systemapi.node.sensors.sensor.SensorService
import org.springframework.stereotype.Service

/**
 * Handling of all operations on Humidity sensors
 */
@Service
class HumidityService (
        humidityDBRepository: HumidityDBRepository,
        nodeService: NodeService
): SensorService<HumidityDB, Int>(
        humidityDBRepository,
        nodeService
) {

    /**
     * Create a new instance for a humidity log
     *
     * @param node  The related Sensor Node
     * @param value The new value from the humidity sensor
     * @return The new log instance
     */
    override fun newInstance(node: NodeDB, value: Int): HumidityDB = HumidityDB(node, value)
}
