--liquibase formatted sql

--changeset agenson:create-light-intensity
CREATE TABLE radl.light_intensity (
    id              BIGSERIAL       NOT NULL    PRIMARY KEY,
    node_id         BIGINT          NOT NULL,
    time            VARCHAR(32)     NOT NULL,
    value           SMALLINT        NOT NULL,
    FOREIGN KEY (node_id) REFERENCES radl.node(id)
);
