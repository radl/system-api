package com.radl.systemapi.node.sensors.sensor

import java.time.OffsetDateTime

/**
 * Converts a SensorDB object to SensorDTO
 */
fun <T> SensorDB<T>.toDTO() = SensorDTO(
        this.time,
        this.value
)

/**
 * DTO version of SensorDB
 *
 * Use a DTO (Data Transfer Object) version of SensorDB
 * to abstract any internal information
 */
class SensorDTO<T> (
        val time: OffsetDateTime,
        val value: T
)
