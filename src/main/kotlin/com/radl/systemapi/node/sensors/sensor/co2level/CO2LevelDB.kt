package com.radl.systemapi.node.sensors.sensor.co2level

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.sensors.sensor.SensorDB
import javax.persistence.Entity
import javax.persistence.Table

/**
 * PostgreSQL 'co2_level' table
 */
@Entity
@Table(name = "co2_level", schema = "radl")
class CO2LevelDB(node: NodeDB, value: Int): SensorDB<Int>(node, value)
