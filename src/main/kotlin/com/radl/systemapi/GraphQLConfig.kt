package com.radl.systemapi

import com.radl.systemapi.utils.graphql.OffsetDateTimeScalarTypeFactory
import com.radl.systemapi.utils.graphql.UuidScalarTypeFactory
import graphql.schema.GraphQLScalarType
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Add scalar type conversions to GraphQL default configuration
 */
@Configuration
class GraphQLConfig {

    /**
     * Conversion between UUID (Kotlin) and String (GraphQL)
     *
     * @param uuidScalarTypeFactory Scalar conversion factory
     * @return A GraphQLScalarType object made from the factory
     */
    @Bean
    internal fun uuidScalarType(
            uuidScalarTypeFactory: UuidScalarTypeFactory
    ): GraphQLScalarType? = uuidScalarTypeFactory.getObject()

    /**
     * Conversion between OffsetDateTime (Kotlin) and String (GraphQL)
     *
     * @param offsetDateTimeScalarTypeFactory Scalar conversion factory
     * @return A GraphQLScalarType object made from the factory
     */
    @Bean
    internal fun offsetDateTimeScalarType(
            offsetDateTimeScalarTypeFactory: OffsetDateTimeScalarTypeFactory
    ): GraphQLScalarType? = offsetDateTimeScalarTypeFactory.getObject()
}
