package com.radl.systemapi.node.sensors.sensor.soundlevel

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.sensors.sensor.SensorDB
import javax.persistence.Entity
import javax.persistence.Table

/**
 * PostgreSQL 'sound_level' table
 */
@Entity
@Table(name = "sound_level", schema = "radl")
class SoundLevelDB(node: NodeDB, value: Int): SensorDB<Int>(node, value)
