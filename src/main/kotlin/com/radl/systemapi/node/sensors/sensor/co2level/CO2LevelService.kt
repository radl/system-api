package com.radl.systemapi.node.sensors.sensor.co2level

import com.radl.systemapi.node.NodeDB
import com.radl.systemapi.node.NodeService
import com.radl.systemapi.node.sensors.sensor.SensorService
import org.springframework.stereotype.Service

/**
 * Handling of all operations on CO2 Level sensors
 */
@Service
class CO2LevelService (
        co2LevelDBRepository: CO2LevelDBRepository,
        nodeService: NodeService
): SensorService<CO2LevelDB, Int>(
        co2LevelDBRepository,
        nodeService
) {

    /**
     * Create a new instance for a CO2 level log
     *
     * @param node  The related Sensor Node
     * @param value The new value from the CO2 level sensor
     * @return The new log instance
     */
    override fun newInstance(node: NodeDB, value: Int): CO2LevelDB = CO2LevelDB(node, value)
}
