package com.radl.systemapi.node.sensors.sensor

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean
import org.springframework.transaction.annotation.Transactional

/**
 * Base of JPA Repository for SQL operation on sensors
 *
 * Needs to be derived with `@Repository` annotation for all sensors
 */
@NoRepositoryBean
interface SensorDBRepository<S: SensorDB<*>>: JpaRepository<S, Long> {

    /**
     * Fetch all the logs from a type of sensor
     *
     * @return All the logs for a type of sensor
     */
    override fun findAll(): List<S>

    /**
     * Fetch the sensor logs from a given a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     * @return The list of sensor logs
     */
    fun findAllByNode_SerialNumber(serialNumber: String): List<S>

    /**
     * Delete all sensor logs from a given a Sensor Node
     *
     * @param serialNumber Unique serial number of the device
     */
    @Transactional
    fun deleteAllByNode_SerialNumber(serialNumber: String)
}
