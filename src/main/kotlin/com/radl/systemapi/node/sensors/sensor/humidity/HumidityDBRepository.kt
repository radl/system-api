package com.radl.systemapi.node.sensors.sensor.humidity

import com.radl.systemapi.node.sensors.sensor.SensorDBRepository
import org.springframework.stereotype.Repository

/**
 * JPA Repository for SQL operation on 'humidity' table (HumidityDB)
 */
@Repository
interface HumidityDBRepository: SensorDBRepository<HumidityDB>
